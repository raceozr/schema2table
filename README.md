# Schema2Table

It's a small and simple CLI application that allows to convert a SQL DDL schema into a human-readable format.

## Get code and prepare to run

### Ubuntu, Debian
```shell
sudo apt-get update
sudo apt-get install python3
cd ~
git clone git@gitlab.com:raceozr/schema2table.git
cd schema2table
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```
### MacOS
1. Clone or download&extract repository files in covinient for you folder
2. Install python3 by loading from [official site](python.org) or using Homebrew `brew install python3`
3. (optinal) Go to project folder, create virtual envoronment and activate it from terminal
    ```shell
    python3 -m venv venv
    source venv/bin/activate
    ```
4. Install project requirements using `pip3 install -r requirements.txt`

### Windows

1. Clone or download&extract repository files in covinient for you folder
2. Install python3 by loading from [official site](python.org)
3. (optinal) Go to project folder, create virtual envoronment and activate it from CMD
    ```console
    python3 -m venv venv
    venv\Scripts\activate.bat
    ```
4. Install project requirements using `pip3 install -r requirements.txt`

## Usage

Simple way:
1. Put `input.sql` with a postgreSQL DDL schema into the project root folder
2. Run `python3 start.py`
3. Enjoy the result in `output.xlsx`

If you want to configurate input/output paths, sql flavor or format, learn more by runnin `python3 start.py -h`.

## Input parameters and features

If you are running script directly, all arguments are optional. All acceptable features and options you can always get using `python3 start.py -h`

## Integrate with your tools

You can connect this script as a module by importing the main functions and using it in the code of the main project.

## Authors and contributing

Initially, this project was developed for personal purposes to simplify work on my tasks at main job.
If you want to support project, feel free to write you wishes and suggestions for improvement on sergey@lyubarsky.info or directly offer your features via merge requests.

## License

This project is under the [GPL3 license](https://www.gnu.org/licenses/gpl-3.0.html)

## Project status

under development
