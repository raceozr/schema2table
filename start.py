from ast import arg
import re
import xlsxwriter
import sys
import re
import argparse

def read_schema(filename: str, sql_flavor: str):
    # read schema
    print('reading data...')
    with open(filename, 'r') as f:
        schema = f.readlines()
    
    # delete commented lines and empty lines
    index = 0
    for _ in range(len(schema)):
        line = schema[index]
        if not line or line.startswith('--'):
            del schema[index]
            continue
        index += 1
    print(f'{len(schema)} function lines loaded')

    tables = {}
    print('finding tables...')
    # read all tables
    current_table = None
    index = 0
    for _ in range(len(schema)):
        line = schema[index]
        # read starts of create query definition
        result = re.match(r'CREATE TABLE (.*) \(', line)
        if result:
            current_table = result.group(1)
            if current_table not in tables:
                tables[current_table] = {}
            del schema[index]
            continue
        # read attributes inside create query
        if current_table:
            result = re.match(r' {4}(.+?) (.+?)(?: DEFAULT (.+?))?(?: (NOT NULL))?(?:,?\n)', line)
            if result:
                tables[current_table][result.group(1)] = {
                    'column': result.group(1),
                    'type': result.group(2),
                    'not null': 'V' if result.group(4) else '',
                    'PK/FK': '',
                    'default': result.group(3) if result.group(3) else '',
                    'comment': '',
                    'reference': ''
                    }
                del schema[index]
                continue
            else:
                # then create query ends
                current_table = None
        index += 1
    print(f'{len(tables)} tables found')

    print('finding constraints...')
    # read all PRIMARY and FOREIGN KEY definitions
    current_table = None
    index = 0
    for _ in range(len(schema)):
        line = schema[index]
        # read starts of alter only query definition
        result = re.match(r'ALTER TABLE ONLY (.*)', line)
        if result:
            current_table = result.group(1)
            del schema[index]
            continue
        # read constraints inside alter
        result = re.match(r' {4}ADD CONSTRAINT.+(PRIMARY KEY|FOREIGN KEY) \((.+?)\)(?: REFERENCES (.+\(.+?\)))?', line)
        if result:
            if result.group(1) == "PRIMARY KEY":
                pkeys = result.group(2).split(', ')
                for pkey in pkeys:
                    tables[current_table][pkey]['PK/FK'] += ', PK' if tables[current_table][pkey]['PK/FK'] else 'PK'
            elif result.group(1) == "FOREIGN KEY":
                tables[current_table][result.group(2)]['PK/FK'] += \
                    ', FK' if tables[current_table][result.group(2)]['PK/FK'] else 'FK'
                tables[current_table][result.group(2)]['reference'] = result.group(3)
            del schema[index]
            continue
        index += 1
    
    print('finding comments...')
    current_table = None
    index = 0
    for _ in range(len(schema)):
        line = schema[index]
        # read comment definition
        result = re.match(r"COMMENT ON COLUMN (?:(.+)\.(.+)) IS '(.+)';", line)
        if result:
            print(result.groups())
            tables[result.group(1)][result.group(2)]['comment'] = result.group(3)
            del schema[index]
            continue
        index += 1

    return tables


def write_info(data:dict, filename: str, output_format: str):
    print('saving data...')\
    # prepare final data
    if output_format == 'xlsx':
        with xlsxwriter.Workbook(filename) as workbook:
            worksheet = workbook.add_worksheet('tables')
            header = ['column', 'type', 'PK/FK', 'reference', 'not null', 'default', 'comment']
            index = 0
            for table in data:
                worksheet.merge_range(index, 0, index, 6, table)
                worksheet.write_row(index + 1, 0, header)
                index += 2
                for i, column in enumerate(data[table]): 
                    worksheet.write_row(index + i, 0, [data[table][column]['column'], data[table][column]['type'],
                        data[table][column]['PK/FK'], data[table][column]['reference'],
                        data[table][column]['not null'], data[table][column]['default'], data[table][column]['comment']])
                index += len(data[table]) + 1

    elif output_format == 'md':
        header = f'column | type | PK/FK | reference | not null | default | comment\n\
            --- | --- | --- | --- | --- | --- | ---'
        lines = []
        for table in data:
            lines.append(f'## {table}')
            lines.append(header)
            for column in data[table]:
                lines.append(f"{data[table][column]['column']} | {data[table][column]['type']} | {data[table][column]['PK/FK']} | \
                    {data[table][column]['reference']} | {data[table][column]['not null']} | {data[table][column]['default']} | \
                    {data[table][column]['comment']}")
            lines.append('')
        with open(filename, 'w') as f:
            for line in lines:
                f.write(line)


    
    


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some schema.')
    parser.add_argument('input', nargs='?', help='filename where schema contains [default: input.sql]', default='input.sql')
    parser.add_argument('output', nargs='?', help='filename that will be creates after procetting [default: output.xlsx]', default='output.xlsx')
    parser.add_argument('schema_flavor', nargs='?', help='which database used for creatind schema [default: postgres]\nsupportable: postgres', default='postgres')
    parser.add_argument('output_format', nargs='?', help='outfut file format [default: xlsx]\nsupportable: md, xlsx', default='xlsx') # , csv, tsv')
    args = parser.parse_args()
    tables = read_schema(args.input, args.schema_flavor)
    write_info(tables, args.output, args.output_format)
    print('done!')